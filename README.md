# XVFB installation Script

Installation script of XVFB for selenium test on RedHat or Centos

## Install XVFB And Selenium
Run the [Xvbf.sh script](Xvfb.sh) to install Xvbf and selenium on the server.

## Configue XVFB on Jenkins
If you use Jenkins, you need to configure the reference of the virtual screen (0 in our case) on the server.
This configuration you can make it as follows:
* Manage Jenkins > Manage node > configure (for the node that you will use, default is Master).
* Check “Environment variables”
* Add the variable

                Name: DISPLAY
                Value: 0


## Issue related to Firefox
If there are any error related to Firefox, try to run the [FireFox.sh script](Firefox.sh) to reinstall Firefox
